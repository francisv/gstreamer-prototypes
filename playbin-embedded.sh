#!/bin/bash
# Runs playbin-embedded

export GST_DEBUG_DUMP_DOT_DIR=/home/francisv/Dropbox/graphs/dots/last
export USE_PLAYBIN3=1

# GST_URI, urisourcebin code needs to add capabilities, features and properties
# parsebin needs properties and features
# apparently playsinkaudioconvert and pulse are not needed or work there is missing
# export GST_DEBUG=latency:9
# export GST_DEBUG=autodetect:4,playsink:4,my_app:4,pulse:3,playbin3:4,playbin:4,GST_REGISTRY:4,GST_ELEMENT_FACTORY:4,decodebin:4,decodebin3:4,GST_URI:4,parsebin:4,urisourcebin:4,playsinkaudioconvert:4
export GST_DEBUG=GST_URI:4,autodetect:4
# export GST_DEBUG=*:9
# export GST_DEBUG=my_app:3
# export GST_DEBUG=playbin:4

## Uncomment for latency tracing
#export GST_DEBUG="GST_TRACER:7"
#export GST_TRACERS=latency
#export G_DEBUG=fatal_warnings


# plays audio from file:
/home/francisv/my-applications/playbin-embedded file:///home/francisv/media/Hydrate-Kenny_Beltrey.ogg

# plays video from file:
#/home/francisv/my-applications/playbin-embedded file:///home/francisv/media/sintel_trailer-480p.webm

# plays video with subtitles from DASH:
# TODO

## GDB debug:

# audio:
# libtool --mode=execute gdb --args /home/francisv/my-applications/playbin-embedded file:///home/francisv/media/Hydrate-Kenny_Beltrey.ogg

# video:
# libtool --mode=execute gdb --args /home/francisv/my-applications/playbin-embedded file:///home/francisv/media/sintel_trailer-480p.webm
