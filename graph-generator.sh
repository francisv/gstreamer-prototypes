#!/bin/sh

DOT_FILES_DIR="~/graphs/latest"
SVG_FILES_DIR="~/graphs/latest"
PNG_FILES_DIR="~/graphs/latest"

DOT_FILES=`ls $DOT_FILES_DIR | grep dot$`

for dot_file in $DOT_FILES
do
    svg_file=`echo $dot_file | sed s/.dot/.svg/`
    eps_file=`echo $dot_file | sed s/.dot/.eps/`
    png_file=`echo $dot_file | sed s/.dot/.png/`
    dot -Tsvg $DOT_FILES_DIR/$dot_file > $SVG_FILES_DIR/$svg_file
    dot -Teps $DOT_FILES_DIR/$dot_file > $PNG_FILES_DIR/$eps_file
    dot -Tpng $DOT_FILES_DIR/$dot_file > $PNG_FILES_DIR/$png_file
done
