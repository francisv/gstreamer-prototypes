/*
  Command to compile in Linux:
  libtool --tag=CC --mode=link --quiet gcc -g -Werror -Wall -Werror=declaration-after-statement -Wno-deprecated-declarations spectrum.c -o spectrum $(pkg-config --cflags --libs gstreamer-1.0 gtk+-3.0)

  Code based on example from:
  https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-good-plugins/html/gst-plugins-good-plugins-spectrum.html#GstSpectrum--bands

 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <gst/gst.h>
#include <gtk/gtk.h>

static guint spect_bands = 20;

#define AUDIOFREQ 32000
#define NAME "simula"
#define GST_CAT_DEFAULT simula
GST_DEBUG_CATEGORY_STATIC (GST_CAT_DEFAULT);

static void
print_graph (GstElement * pipeline)
{
  gchar *file_name;
  GFileIOStream *iostream = NULL;
  GError *error = NULL;
  GFile *file = g_file_new_tmp ("graph_XXXXXX", &iostream, &error);
  gchar *data;
  gsize bytes_written;

  if (!file) {
    GST_ERROR ("Error creating file: %s", error->message);
    g_assert_not_reached ();
  }

  data = gst_debug_bin_to_dot_data (GST_BIN (pipeline),
                                    GST_DEBUG_GRAPH_SHOW_ALL);

  if (!g_output_stream_write_all (
                                  g_io_stream_get_output_stream (
                                                                 G_IO_STREAM (iostream)),
                                  data,
                                  strlen (data), &bytes_written, NULL,
                                  &error)) {
    GST_ERROR ("Error while writing dot file: %s", error->message);
    g_assert_not_reached ();
  }

  g_free (data);

  file_name = g_file_get_path (file);

  g_object_unref (iostream);
  g_object_unref (file);

  GST_INFO ("File is: %s", file_name);

  /* FIXME: Catch error when `xdot' is not installed */
  if (fork () == 0) {
    execlp ("xdot", "xdot", file_name, NULL);
    exit (0);
  }

  g_free (file_name);
}

/* receive spectral data from element message */
static gboolean
message_handler (GstBus * bus, GstMessage * message, gpointer user_data)
{
  GstElement *pipeline = user_data;
  if (message->type == GST_MESSAGE_ELEMENT) {
    const GstStructure *s = gst_message_get_structure (message);
    const gchar *name = gst_structure_get_name (s);
    GstClockTime endtime;

    if (strcmp (name, "spectrum") == 0) {
      const GValue *magnitudes;
      const GValue *phases;
      const GValue *mag, *phase;
      gdouble freq;
      guint i;

      if (!gst_structure_get_clock_time (s, "endtime", &endtime))
        endtime = GST_CLOCK_TIME_NONE;

      g_print ("New spectrum message, endtime %" GST_TIME_FORMAT "\n",
          GST_TIME_ARGS (endtime));

      magnitudes = gst_structure_get_value (s, "magnitude");
      phases = gst_structure_get_value (s, "phase");

      for (i = 0; i < spect_bands; ++i) {
        freq = (gdouble) ((AUDIOFREQ / 2) * i + AUDIOFREQ / 4) / spect_bands;
        mag = gst_value_list_get_value (magnitudes, i);
        phase = gst_value_list_get_value (phases, i);

        if (mag != NULL && phase != NULL) {
          g_print ("band %d (freq %g): magnitude %f dB phase %f\n", i, freq,
              g_value_get_float (mag), g_value_get_float (phase));
        }
      }
      g_print ("\n");
    }
  }
  if (GST_MESSAGE_TYPE (message) == GST_MESSAGE_ASYNC_DONE){
    print_graph(pipeline);
  }
  return TRUE;
}

int
main (int argc, char *argv[])
{
  GstElement *bin;
  GstElement *src, *wavparse, *spectrum, *sink;
  /* GstElement *audioconvert; */
  GstBus *bus;
  GstCaps *caps;
  GMainLoop *loop;

  gst_init (&argc, &argv);
  GST_DEBUG_CATEGORY_INIT (GST_CAT_DEFAULT, NAME, 0, NAME);

  bin = gst_pipeline_new ("bin");

  src = gst_element_factory_make ("filesrc", "src");
  g_object_set (G_OBJECT (src), "location", "/home/francisv/gitlab/simula-hackathon/src/01_Kick.wav", NULL);
  /* audioconvert = gst_element_factory_make ("audioconvert", NULL); */
  /* g_assert (audioconvert); */

  wavparse = gst_element_factory_make ("wavparse", NULL);
  g_assert (wavparse);

  spectrum = gst_element_factory_make ("spectrum", "spectrum");
  g_object_set (G_OBJECT (spectrum), "bands", spect_bands, "threshold", -80,
      "post-messages", TRUE, "message-phase", TRUE, NULL);

  sink = gst_element_factory_make ("pulsesink", "sink");
  g_object_set (G_OBJECT (sink), "sync", TRUE, NULL);

  gst_bin_add_many (GST_BIN (bin), src, wavparse, spectrum, sink, NULL);

  caps = gst_caps_new_simple ("audio/x-raw",
      "rate", G_TYPE_INT, AUDIOFREQ, NULL);

  if (!gst_element_link (src, wavparse) ||
      !gst_element_link_filtered (wavparse, spectrum, caps) ||
      !gst_element_link (spectrum, sink)) {
    fprintf (stderr, "can't link elements\n");
    exit (1);
  }
  gst_caps_unref (caps);

  bus = gst_element_get_bus (bin);
  gst_bus_add_watch (bus, message_handler, bin);
  gst_object_unref (bus);

  gst_element_set_state (bin, GST_STATE_PLAYING);

  /* we need to run a GLib main loop to get the messages */
  loop = g_main_loop_new (NULL, FALSE);
  g_main_loop_run (loop);

  gst_element_set_state (bin, GST_STATE_NULL);

  gst_object_unref (bin);

  return 0;
}
