/* - Purpose: Videoconferencing application to measure how much time it takes to
   build and reconfigure its pipeline

   - Compile:

   libtool --tag=CC --mode=link --quiet gcc -g -Werror -Wall -Werror=declaration-after-statement -Wno-deprecated-declarations videoconferencing-application.c -o videoconferencing-application $(pkg-config --cflags --libs gstreamer-1.0 gstreamer-video-1.0-uninstalled gtk+-3.0)

   - GDB:

   GDK_BACKEND=x11 libtool --mode=execute gdb -i=mi videoconferencing-application

   - Use:

   GDK_BACKEND=x11 GST_DEBUG=DAMPAT:9 ./videoconferencing-application


   XXX: I should add a guard like in
   https://developer.gnome.org/gdk3/stable/gdk3-X-Window-System-Interaction.html

   This code is based on https://github.com/jcaden/gst-dynamic-examples

*/

#include <gst/gst-i18n-app.h>
#include <memory.h>
#include <gst/video/videooverlay.h>
#include <gdk/gdkx.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <gst/gst.h>

/* Plan phase */
/* Create each path with functional stages when it applicable*/
/* Results from these experiments go into one table */
/* I still have to get the number of elements per functional stage*/

#define WEBCAM_ONLY TRUE  // video from webcam only
#define CC_ONLY FALSE  // cc only (there are no functional stages here)
#define TX_ONLY FALSE // tx only (there are no functional stages here)

/* Execution phase */
/* Reconfiguration time is tested by clicking the button manually for the time being */
/* Results of these experiments and manual button clicking go into another table */
#define WEBCAM_ONLY_NON_FS FALSE  // video from webcam only
#define WEBCAM_CC_NON_FS FALSE
#define WEBCAM_TX_NON_FS FALSE
#define WEBCAM_CC_TX_NON_FS FALSE

/*******/

#define SRC_NAME "src"
#define FILTER_NAME "close-captioning"
#define TEE_NAME "splitter"
#define FILE_SINK_NAME "file_sink"

#define START_TRANSMISSION "Start transmission"
#define STOP_TRANSMISSION "Stop transmission"
#define FILE_LOCATION "/tmp/test.mkv"

#define ADD_CLOCK_OVERLAY "Close captioning"
#define REMOVE_CLOCK_OVERLAY "No CC"

#define WINDOW_WIDTH 400
#define WINDOW_HEIGHT 400

#define NAME "DAMPAT"
#define GST_CAT_DEFAULT videconferencing
GST_DEBUG_CATEGORY_STATIC (GST_CAT_DEFAULT);

#define APP_DATA_INIT {0, NULL, FALSE, NULL, NULL, NULL, FALSE, NULL}

typedef struct _AppData
{
  guintptr video_window_handle;
  GstElement *pipeline;
  gboolean filter_set;
  GtkWidget *status_bar;
  GtkWidget *filter_button;
  GtkWidget *transmission_button;
  gboolean transmission;
  GstPad *transmission_sink;
  GstClockTime start, end;
  GstClockTimeDiff diff;
} AppData;

static gboolean
update_transmission_widgets_status (AppData *app_data)
{
  const gchar *message;

  if (g_atomic_int_get (&app_data->transmission)) {
    message = "Transmission started correctly";
  } else {
    message = "Transmission stopped correctly";
    /* This is always modified from gui thread */
    g_clear_object (&app_data->transmission_sink);
  }
  gtk_statusbar_push (GTK_STATUSBAR (app_data->status_bar), 0,
                      message);
  gtk_widget_set_sensitive (app_data->transmission_button, TRUE);

  return G_SOURCE_REMOVE;
}

static void
start_transmission (AppData *app_data)
{
  GError *error = NULL;
  GstPad *tee_src;
  GstElement *tee, *transmission_bin;

  app_data->start = gst_util_get_timestamp();

  tee = gst_bin_get_by_name (GST_BIN (app_data->pipeline), TEE_NAME);
  g_assert (tee);
  tee_src = gst_element_get_request_pad (tee, "src_%u");
  g_assert (tee_src);

  /* We add a videoconvert to handle video conversions if required as camera
   * caps are fixed */
  /* Vp8enc is configured for real time otherwise the buffers will be delayed */
  transmission_bin = gst_parse_bin_from_description (
                                                     "queue max-size-buffers=0 ! videoconvert !"
                                                     " vp8enc deadline=1 threads=1 ! matroskamux !"
                                                     " filesink name=" FILE_SINK_NAME " sync=false location=" FILE_LOCATION,
                                                     TRUE, &error);
  if (transmission_bin == NULL) {
    GST_ERROR ("Error creating bin: %s", error->message);
    g_clear_error (&error);
    g_assert_not_reached ();
  }

  gst_bin_add (GST_BIN (app_data->pipeline), transmission_bin);
  gst_element_sync_state_with_parent (transmission_bin);

  g_assert (
            gst_element_link_pads (tee, GST_OBJECT_NAME (tee_src), transmission_bin,
                                   NULL));

  if (app_data->transmission_sink) {
    GST_ERROR ("Transmission sink is already set, this should not happen");
    g_assert (!app_data->transmission_sink);
  }

  /* This is always modified from gui thread */
  app_data->transmission_sink = gst_pad_get_peer (tee_src);

  g_object_unref (tee_src);
  g_object_unref (tee);

  g_atomic_int_set (&app_data->transmission, TRUE);
  g_idle_add ((GSourceFunc) update_transmission_widgets_status, app_data);
}

static gboolean
release_transmission_bin (gpointer transmission_bin)
{
  gst_element_set_state (transmission_bin, GST_STATE_NULL);

  return G_SOURCE_REMOVE;
}

static GstPadProbeReturn
filesink_eos_probe (GstPad *tee_src, GstPadProbeInfo *info, gpointer data)
{
  GstElement *transmission_bin;
  AppData *app_data = data;
  GstEvent *event = GST_PAD_PROBE_INFO_EVENT (info);

  if (GST_EVENT_TYPE (event) != GST_EVENT_EOS) {
    return GST_PAD_PROBE_OK;
  }

  transmission_bin = gst_pad_get_parent_element (app_data->transmission_sink);
  g_assert (transmission_bin);

  gst_bin_remove (GST_BIN (app_data->pipeline), transmission_bin);

  /* State should be changed from a different thread */
  g_idle_add_full (G_PRIORITY_DEFAULT, release_transmission_bin, transmission_bin,
                   g_object_unref);

  g_atomic_int_set (&app_data->transmission, FALSE);
  g_idle_add ((GSourceFunc) update_transmission_widgets_status, app_data);

  app_data->end = gst_util_get_timestamp ();
  app_data->diff = GST_CLOCK_DIFF (app_data->start, app_data->end);
  g_print (_("Removed TX filesink_eos_probe: %" GST_TIME_FORMAT "\n"),
           GST_TIME_ARGS (app_data->diff));

  return GST_PAD_PROBE_REMOVE;
}

static GstPadProbeReturn
stop_transmission_probe (GstPad *tee_src, GstPadProbeInfo *info, gpointer data)
{
  AppData *app_data = data;
  GstElement *tee, *filesink;
  GstPad *filesink_sink;

  g_assert (app_data->transmission_sink);

  gst_pad_unlink (tee_src, app_data->transmission_sink);

  filesink = gst_bin_get_by_name_recurse_up (GST_BIN (app_data->pipeline),
                                             FILE_SINK_NAME);
  g_assert (filesink);
  filesink_sink = gst_element_get_static_pad (filesink, "sink");
  g_assert (filesink_sink);

  /* Send eos event to finish file correctly */
  gst_pad_add_probe (filesink_sink, GST_PAD_PROBE_TYPE_EVENT_DOWNSTREAM,
                     filesink_eos_probe, app_data, NULL);
  gst_pad_send_event (app_data->transmission_sink, gst_event_new_eos ());

  g_object_unref (filesink_sink);
  g_object_unref (filesink);

  tee = gst_pad_get_parent_element (tee_src);
  g_assert (tee);
  gst_element_remove_pad (tee, tee_src);
  g_object_unref (tee);

  app_data->end = gst_util_get_timestamp ();
  app_data->diff = GST_CLOCK_DIFF (app_data->start, app_data->end);
  /* g_print (_("stop_transmission_probe: %" GST_TIME_FORMAT "\n"), */
  /*          GST_TIME_ARGS (app_data->diff)); */

  return GST_PAD_PROBE_REMOVE;
}

static void
stop_transmission (AppData *app_data)
{
  GstPad *tee_src;

  app_data->start = gst_util_get_timestamp();

  if (!app_data->transmission_sink) {
    GST_ERROR ("Transmission sink is not set, this should not happen");
    g_assert (app_data->transmission_sink);
  }

  tee_src = gst_pad_get_peer (app_data->transmission_sink);
  g_assert (tee_src);

  gst_pad_add_probe (tee_src, GST_PAD_PROBE_TYPE_IDLE, stop_transmission_probe,
                     app_data, NULL);

  g_object_unref (tee_src);
}

static gboolean
update_filter_widgets_status (AppData *app_data)
{
  const gchar *message;

  if (g_atomic_int_get (&app_data->filter_set)) {
    message = "Close captioning added correctly";
  } else {
    message = "Close captioning removed correctly";
  }
  gtk_statusbar_push (GTK_STATUSBAR (app_data->status_bar), 0,
                      message);
  gtk_widget_set_sensitive (app_data->filter_button, TRUE);

  return G_SOURCE_REMOVE;
}

static GstPadProbeReturn
disconnect_element_probe (GstPad *src_peer, GstPadProbeInfo *info,
                          gpointer data)
{
  GstPad *src_pad, *sink_peer, *sink_pad;
  GstElement *filter;
  AppData *app_data = data;

  sink_pad = gst_pad_get_peer (src_peer);
  g_assert (sink_pad);

  filter = gst_pad_get_parent_element (sink_pad);
  g_assert (filter);

  src_pad = gst_element_get_static_pad (filter, "src");
  sink_peer = gst_pad_get_peer (src_pad);
  g_assert (sink_peer);

  gst_pad_unlink (src_peer, sink_pad);
  gst_pad_unlink (src_pad, sink_peer);
  gst_pad_link (src_peer, sink_peer);

  gst_element_set_state (filter, GST_STATE_NULL);
  gst_bin_remove (GST_BIN (app_data->pipeline), filter);

  g_object_unref (sink_peer);
  g_object_unref (src_pad);
  g_object_unref (sink_pad);
  g_object_unref (filter);

  app_data->end = gst_util_get_timestamp ();
  app_data->diff = GST_CLOCK_DIFF (app_data->start, app_data->end);
  g_print (_("Removed CC: %" GST_TIME_FORMAT "\n"),
           GST_TIME_ARGS (app_data->diff));

  GST_DEBUG ("Filter removed correctly");

  g_atomic_int_set (&app_data->filter_set, FALSE);
  g_idle_add ((GSourceFunc) update_filter_widgets_status, app_data);

  return GST_PAD_PROBE_REMOVE;
}

static void
disconnect_filter (AppData *app_data)
{
  GstPad *sink_pad, *src_peer;
  GstElement
    *filter = gst_bin_get_by_name (GST_BIN (app_data->pipeline), FILTER_NAME);

  app_data->start = gst_util_get_timestamp();

  g_assert (filter);

  sink_pad = gst_element_get_static_pad (filter, "video_sink");
  g_assert (sink_pad);
  src_peer = gst_pad_get_peer (sink_pad);
  g_assert (src_peer);

  /* Note that we are waiting for the src pad to be idle, otherwise it can emit
   * buffers while the pads are disconnected.  If calling without blocking it
   * may fail */
  gst_pad_add_probe (src_peer, GST_PAD_PROBE_TYPE_IDLE,
                     disconnect_element_probe, app_data, NULL);

  g_object_unref (src_peer);
  g_object_unref (sink_pad);
  g_object_unref (filter);
}

static GstPadProbeReturn
connect_element_probe (GstPad *pad, GstPadProbeInfo *info, gpointer data)
{
  AppData *app_data = data;
  GstElement *filter;
  GstPad *peer;

  peer = gst_pad_get_peer (pad);
  if (peer == NULL) {
    return GST_PAD_PROBE_REMOVE;
  }

  GST_DEBUG ("Adding close captioning...");
  /* Unlink pads */
  gst_pad_unlink (pad, peer);

  /* Create element (substitution of close captioning) */
  filter = gst_element_factory_make ("clockoverlay", FILTER_NAME);
  g_object_set (filter, "font-desc", "Arial 32", NULL);
  g_object_set (filter, "halignment", 1, NULL);
  g_object_set (filter, "valignment", 1, NULL);
  gst_bin_add (GST_BIN (app_data->pipeline), filter);
  gst_element_sync_state_with_parent (filter);

  /* Connect new element */
  gst_element_link_pads (GST_ELEMENT (GST_OBJECT_PARENT (pad)),
                         GST_OBJECT_NAME (pad), filter, NULL);
  gst_element_link_pads (filter, NULL, GST_ELEMENT (GST_OBJECT_PARENT (peer)),
                         GST_OBJECT_NAME (peer));


  app_data->end = gst_util_get_timestamp ();
  app_data->diff = GST_CLOCK_DIFF (app_data->start, app_data->end);
  g_print (_("Added CC: %" GST_TIME_FORMAT "\n"),
           GST_TIME_ARGS (app_data->diff));

  g_object_unref (peer);

  GST_DEBUG ("Close captioning added correctly");

  g_atomic_int_set (&app_data->filter_set, TRUE);
  g_idle_add ((GSourceFunc) update_filter_widgets_status, app_data);

  return GST_PAD_PROBE_REMOVE;
}

static void
connect_new_filter (AppData *app_data)
{
  GstPad *src_pad;
  GstElement *src;

  app_data->start = gst_util_get_timestamp();

  src = gst_bin_get_by_name (GST_BIN (app_data->pipeline), SRC_NAME);
  g_assert (src);

  src_pad = gst_element_get_static_pad (src, "src");
  g_assert (src_pad);

  /* Trying to execute `connect_element_probe' directly may fail depending on
     race conditions */
  gst_pad_add_probe (src_pad, GST_PAD_PROBE_TYPE_IDLE, connect_element_probe,
                     app_data, NULL);

  g_object_unref (src_pad);
  g_object_unref (src);
}

static void
filter_button_clicked (AppData *app_data)
{
  gtk_widget_set_sensitive (app_data->filter_button, FALSE);

  if (g_atomic_int_get (&app_data->filter_set)) {
    gtk_button_set_label (GTK_BUTTON (app_data->filter_button),
                          ADD_CLOCK_OVERLAY);
    disconnect_filter (app_data);
  } else {
    gtk_button_set_label (GTK_BUTTON (app_data->filter_button),
                          REMOVE_CLOCK_OVERLAY);
    connect_new_filter (app_data);
  }
}

static void
graph_button_clicked (AppData *app_data)
{
  gchar *file_name;
  GFileIOStream *iostream = NULL;
  GError *error = NULL;
  GFile *file = g_file_new_tmp ("graph_XXXXXX", &iostream, &error);
  gchar *data;
  gsize bytes_written;

  if (!file) {
    GST_ERROR ("Error creating file: %s", error->message);
    g_assert_not_reached ();
  }

  data = gst_debug_bin_to_dot_data (GST_BIN (app_data->pipeline),
                                    GST_DEBUG_GRAPH_SHOW_ALL);

  if (!g_output_stream_write_all (
                                  g_io_stream_get_output_stream (
                                                                 G_IO_STREAM (iostream)),
                                  data,
                                  strlen (data), &bytes_written, NULL,
                                  &error)) {
    GST_ERROR ("Error while writing dot file: %s", error->message);
    g_assert_not_reached ();
  }

  g_free (data);

  file_name = g_file_get_path (file);

  g_object_unref (iostream);
  g_object_unref (file);

  GST_INFO ("File is: %s", file_name);

  /* FIXME: Catch error when `xdot' is not installed */
  if (fork () == 0) {
    execlp ("xdot", "xdot", file_name, NULL);
    exit (0);
  }

  g_free (file_name);
}

static GstBusSyncReply
bus_sync_handler (GstBus *bus, GstMessage *message, gpointer user_data)
{
  AppData *app_data = user_data;

  // ignore anything but `prepare-window-handle' element messages
  if (!gst_is_video_overlay_prepare_window_handle_message (message)) {
    if (GST_MESSAGE_TYPE (message) == GST_MESSAGE_ASYNC_DONE){
      /* Get timestamp to know how much time it took the pipeline to be ready */
      app_data->end = gst_util_get_timestamp ();
      app_data->diff = GST_CLOCK_DIFF (app_data->start, app_data->end);
      g_print (_("Added TX: %" GST_TIME_FORMAT "\n"),
               GST_TIME_ARGS (app_data->diff));

      /* I got all what I wanted.  Let's quit the application */
      g_assert(NULL);
    }
    else if (GST_MESSAGE_TYPE (message) == GST_MESSAGE_ERROR) {
      GError *error;
      gchar *debug;

      gst_message_parse_error (message, &error, &debug);
      GST_ERROR ("ERROR from element %s: %s",
                 GST_OBJECT_NAME (message->src), error->message);
      if (debug) {
        GST_ERROR ("Debugging info: %s", debug);
      }
      g_error_free (error);
      g_free (debug);

      g_assert_not_reached ();
    } else if (GST_MESSAGE_TYPE (message) == GST_MESSAGE_WARNING) {
      GST_ERROR ("Warning on bug: %"
                 GST_PTR_FORMAT, message);
    }
    return GST_BUS_PASS;
  }

  if (app_data->video_window_handle != 0) {
    GstVideoOverlay *overlay;

    overlay = GST_VIDEO_OVERLAY (GST_MESSAGE_SRC (message));
    gst_video_overlay_set_window_handle (overlay,
                                         app_data->video_window_handle);
  } else {
    g_warning ("Should have obtained video_window_handle by now!");
  }

  gst_message_unref (message);
  return GST_BUS_DROP;
}

static void
create_pipeline (AppData *app_data)
{
  GError *err = NULL;
  GstBus *bus;
  GstElement *pipeline;

  app_data->start = gst_util_get_timestamp();

  /* We add a capsfilter to avoid renegotiation that could stop the camera */
#if WEBCAM_ONLY
  pipeline = gst_parse_launch (
                               "autovideosrc name=" SRC_NAME
                               " ! video/x-raw,format=(string)YUY2 ! "
                               "tee name=" TEE_NAME
                               " ! queue ! autovideosink name=sink", &err);
#endif

#if CC_ONLY
  pipeline = gst_parse_launch (
                               "videotestsrc pattern=2 name=" SRC_NAME
                               " ! clockoverlay font-desc=\"Arial 32\" halignment=1 valignment=1"
                               " ! video/x-raw,format=(string)YUY2 ! "
                               "tee name=" TEE_NAME
                               " ! queue ! autovideosink name=sink", &err);
#endif

#if TX_ONLY
  pipeline = gst_parse_launch (
                               "autovideosrc name=" SRC_NAME " ! video/x-raw,format=YUY2 ! tee name=" TEE_NAME " ! queue max-size-buffers=0 ! videoconvert ! vp8enc deadline=1 threads=1 ! matroskamux ! filesink name=" FILE_SINK_NAME " sync=false location=" FILE_LOCATION,
                               &err);
#endif

#if WEBCAM_ONLY_NON_FS
  pipeline = gst_parse_launch (
                               "v4l2src name=" SRC_NAME
                               " ! video/x-raw,format=(string)YUY2 ! "
                               "tee name=" TEE_NAME
                               " ! queue ! glimagesink name=sink", &err);
#endif

#if WEBCAM_CC_NON_FS
  pipeline = gst_parse_launch (
                               "v4l2src name=" SRC_NAME
                               " ! clockoverlay font-desc=\"Arial 32\" halignment=1 valignment=1"
                               " ! video/x-raw,format=(string)YUY2 ! "
                               "tee name=" TEE_NAME
                               " ! queue ! glimagesink name=sink", &err);
#endif

#if WEBCAM_TX_NON_FS
  pipeline = gst_parse_launch (
                               "v4l2src name=" SRC_NAME " ! video/x-raw,format=YUY2 ! tee name=" TEE_NAME " ! queue ! glimagesink name=sink, "
                               TEE_NAME ". ! queue max-size-buffers=0 ! videoconvert ! vp8enc deadline=1 threads=1 ! matroskamux ! filesink name=" FILE_SINK_NAME " sync=false location=" FILE_LOCATION,
                               &err);
#endif

#if WEBCAM_CC_TX_NON_FS
  pipeline = gst_parse_launch (
                               "v4l2src name=" SRC_NAME
                               " ! clockoverlay font-desc=\"Arial 32\" halignment=1 valignment=1"
                               " ! video/x-raw,format=YUY2 ! tee name=" TEE_NAME " ! queue ! glimagesink name=sink "
                               TEE_NAME ". ! queue max-size-buffers=0 ! videoconvert ! vp8enc deadline=1 threads=1 ! matroskamux ! filesink name=" FILE_SINK_NAME " sync=false location=" FILE_LOCATION,
                               &err);
#endif

  if (pipeline == NULL) {
    GST_ERROR ("Error while creating the pipeline: %s", err->message);
    g_error_free (err);
    g_assert_not_reached ();
  }

  bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
  gst_bus_set_sync_handler (bus, (GstBusSyncHandler) bus_sync_handler, app_data,
                            NULL);
  gst_object_unref (bus);

  gst_element_set_state (pipeline, GST_STATE_PLAYING);

  app_data->pipeline = pipeline;
}

static void
video_widget_realize_cb (GtkWidget *widget, gpointer data)
{
  AppData *app_data = data;

  app_data->video_window_handle =
    GDK_WINDOW_XID (gtk_widget_get_window (widget));
}

static void
transmission_button_clicked (AppData *app_data)
{
  gtk_widget_set_sensitive (app_data->transmission_button, FALSE);

  if (g_atomic_int_get (&app_data->transmission)) {
    gtk_button_set_label (GTK_BUTTON (app_data->transmission_button),
                          START_TRANSMISSION);
    stop_transmission (app_data);
  } else {
    gtk_button_set_label (GTK_BUTTON (app_data->transmission_button),
                          STOP_TRANSMISSION);
    start_transmission (app_data);
  }
}

static void
activate_gui (GtkApplication *app, gpointer user_data)
{
  GtkWidget *window, *window_content, *button_box, *filter_button,
    *transmission_button, *graph_button, *video_widget, *status_bar;
  AppData *app_data = user_data;

  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "DAMPAT Videoconferencing");
  gtk_window_set_default_size (GTK_WINDOW (window), WINDOW_WIDTH,
                               WINDOW_HEIGHT);

  window_content = gtk_box_new (GTK_ORIENTATION_VERTICAL, 4);
  gtk_container_add (GTK_CONTAINER (window), window_content);

  button_box = gtk_button_box_new (GTK_ORIENTATION_HORIZONTAL);
  gtk_container_add (GTK_CONTAINER (window_content), button_box);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (button_box), GTK_BUTTONBOX_EXPAND);

  filter_button = gtk_button_new_with_label (ADD_CLOCK_OVERLAY);
  g_signal_connect_swapped (filter_button, "clicked",
                            G_CALLBACK (filter_button_clicked), app_data);
  gtk_container_add (GTK_CONTAINER (button_box), filter_button);
  app_data->filter_button = filter_button;

  transmission_button = gtk_button_new_with_label (START_TRANSMISSION);
  g_signal_connect_swapped (transmission_button, "clicked",
                            G_CALLBACK (transmission_button_clicked), app_data);
  gtk_container_add (GTK_CONTAINER (button_box), transmission_button);
  app_data->transmission_button = transmission_button;

  graph_button = gtk_button_new_with_label ("Generate graph");
  g_signal_connect_swapped (graph_button, "clicked",
                            G_CALLBACK (graph_button_clicked), app_data);
  gtk_container_add (GTK_CONTAINER (button_box), graph_button);

  video_widget = gtk_drawing_area_new ();
  g_signal_connect (video_widget, "realize",
                    G_CALLBACK (video_widget_realize_cb), app_data);
  gtk_container_add (GTK_CONTAINER (window_content), video_widget);
  gtk_box_set_child_packing (GTK_BOX (window_content), video_widget, TRUE, TRUE,
                             1, GTK_PACK_START);

  status_bar = gtk_statusbar_new ();
  gtk_container_add (GTK_CONTAINER (window_content), status_bar);
  app_data->status_bar = status_bar;

  gtk_widget_show_all (window);
  gtk_widget_realize (video_widget);

  g_assert (app_data->video_window_handle != 0);

  create_pipeline (app_data);
}

static void
kill_children ()
{
  kill ((pid_t) 0, SIGINT);
}

int
main (int argc, char **argv)
{
  GtkApplication *app;
  int status;
  AppData app_data = APP_DATA_INIT;
  GOptionGroup *gst_group;

  atexit (kill_children);

  gst_init (&argc, &argv);
  gst_group = gst_init_get_option_group ();

  GST_DEBUG_CATEGORY_INIT (GST_CAT_DEFAULT, NAME, 0, NAME);

  app = gtk_application_new ("DAMPAT.videoconferencing",
                             G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate_gui), &app_data);

  g_application_add_option_group (G_APPLICATION (app), gst_group);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  if (app_data.transmission_sink != NULL) {
    g_clear_object (&app_data.transmission_sink);
  }

  if (app_data.pipeline != NULL) {
    gst_element_send_event (app_data.pipeline, gst_event_new_eos ());
    gst_element_set_state (app_data.pipeline, GST_STATE_NULL);
    g_clear_object (&app_data.pipeline);
  }

  gst_deinit ();

  return status;
}
