/*
  Purpose:
  Analyze when capabilities are created at decoder's source pad

  Usage:
  The file to be read is harcoded in the code

  Compile:
  gcc -g -O0 newcaps.c -o newcaps $(pkg-config --cflags --libs gstreamer-1.0)

  Run:
  GST_DEBUG=newcaps:5 /home/francisv/my-applications/newcaps

  Results:
  Linking does not create define the capabilities of decoder's source pad.
  The callback from notify::caps signal does tell the defined capabilities

*/

#include <gst/gst.h>

GST_DEBUG_CATEGORY_STATIC (my_category);
#define GST_CAT_DEFAULT my_category

/* Structure to contain all our information, so we can pass it to callbacks */
typedef struct _CustomData{
  GMainLoop *loop;
  GMainContext *context; /* GLib context used to run the main loop */
  GstElement *pipeline;
  GstElement *source;
  GstElement *demuxer;
  GstElement *decoder;
  GstElement *sink;
} CustomData;

static GstCaps *
get_pad_caps (GstPad * pad)
{
  GstCaps *caps;

  /* first check the pad caps, if this is set, we are positively sure it is
   * fixed and exactly what the element will produce. */
  caps = gst_pad_get_current_caps (GST_PAD(pad));

  /* then use the getcaps function if we don't have caps. These caps might not
   * be fixed in some cases, in which case analyze_new_pad will set up a
   * notify::caps signal to continue autoplugging. */
  if (caps == NULL)
    caps = gst_pad_query_caps (GST_PAD(pad), NULL);

  return caps;
}

/* Links demuxer -> decoder */
static void on_pad_added(GstElement *element, GstPad *pad, CustomData *data){
  GstPad *sinkpad;
  GstPad *srcpad;
  GstPadLinkReturn ret;
  GstCaps *caps;

  sinkpad = gst_element_get_static_pad(data->decoder, "sink");
  srcpad = gst_element_get_static_pad(data->decoder, "src");

  /* caps of decoder's source pad
   * This is to test whether those capabilities changed after linking
   */
  caps = get_pad_caps (srcpad);

  GST_DEBUG_OBJECT (data->decoder,
                    "Before linking - Pad %s:%s caps:%" GST_PTR_FORMAT,
                    GST_DEBUG_PAD_NAME (srcpad), caps);

  ret = gst_pad_link(pad, sinkpad);
  if (!GST_PAD_LINK_SUCCESSFUL (ret)) {
    GST_ERROR ("Failed to link %s:%s to %s:%s (ret = %d)\n",
               GST_DEBUG_PAD_NAME (pad), GST_DEBUG_PAD_NAME (sinkpad), ret);
  } else {
    GST_DEBUG ("Linked %s:%s to %s:%s\n", GST_DEBUG_PAD_NAME (pad),
               GST_DEBUG_PAD_NAME (sinkpad));
  }

  /* caps of decoder's source pad
   * This is to test whether those capabilities changed after linking
   */
  caps = get_pad_caps (srcpad);
  GST_DEBUG_OBJECT (data->decoder,
                    "After linking - Pad %s:%s caps:%" GST_PTR_FORMAT,
                    GST_DEBUG_PAD_NAME (srcpad), caps);

  gst_object_unref(sinkpad);
}

/* Displays capabilities of decoder's source pad
 * when signal notify::caps appears
 */
static void
notify_caps_cb(GstPad *pad, GParamSpec * arg G_GNUC_UNUSED,
               CustomData *data){
  GstCaps *caps;

  caps = get_pad_caps (pad);
  GST_DEBUG_OBJECT (pad,
                    "Notify caps - Pad %s:%s caps:%" GST_PTR_FORMAT,
                    GST_DEBUG_PAD_NAME (pad), caps);
}

/* Reviews probes in decoder's source pad */
static GstPadProbeReturn
pad_probe_cb(GstPad * pad, GstPadProbeInfo * info, CustomData *data){
  GstCaps *caps;

  GstEvent *event = GST_PAD_PROBE_INFO_EVENT(info);

  GST_LOG_OBJECT(pad, "Probe type: %s", GST_EVENT_TYPE_NAME(event));

  caps = get_pad_caps (pad);
  GST_LOG_OBJECT (pad,
                  "Probe callback - Pad %s:%s caps:%" GST_PTR_FORMAT,
                  GST_DEBUG_PAD_NAME (pad), caps);
  return GST_PAD_PROBE_OK;
}

gint main (gint argc, gchar *argv[]){
  CustomData data;
  GstPad *srcpad;
  guint bus_watch_id;

  GST_DEBUG_CATEGORY_INIT (my_category, "newcaps", 0,
                           "This is the debug category for my code.");

  gst_init(&argc, &argv);

  /* create pipeline */
  data.pipeline = gst_pipeline_new("my_pipeline");
  data.source = gst_element_factory_make ("filesrc", "source");
  data.demuxer = gst_element_factory_make ("oggdemux", "demuxer");
  data.decoder = gst_element_factory_make ("vorbisdec", "decoder");
  data.sink = gst_element_factory_make ("pulsesink", "sink");

  if (!data.pipeline){
    g_printerr("pipeline element could not be created. Exiting.\n");
    return -1;
  }else if (!data.source){
    g_printerr("source element could not be created. Exiting.\n");
    return -1;
  }else if (!data.demuxer){
    g_printerr("demuxer element could not be created. Exiting.\n");
    return -1;
  }else if (!data.decoder){
    g_printerr("decoder element could not be created. Exiting.\n");
    return -1;
  }else if (!data.sink){
    g_printerr("sink element could not be created. Exiting.\n");
    return -1;
  }

  /* Set the input filename to the source element */
  g_object_set(data.source, "location",
               "/home/francisv/media/Hydrate-Kenny_Beltrey.ogg", NULL);

  gst_bin_add_many(GST_BIN(data.pipeline), data.source, data.demuxer,
                   data.decoder, data.sink, NULL);

  gst_element_link(data.source,data.demuxer);
  gst_element_link_many(data.decoder,data.sink,NULL);

  g_signal_connect(data.demuxer, "pad-added", G_CALLBACK(on_pad_added),
                   &data);

  srcpad = gst_element_get_static_pad(data.decoder, "src");

  g_signal_connect(srcpad, "notify::caps", G_CALLBACK(notify_caps_cb),
                   &data);

  /* Add different types of probes to the decoder's source pad*/
  gst_pad_add_probe(srcpad, GST_PAD_PROBE_TYPE_EVENT_BOTH,
                    (GstPadProbeCallback) pad_probe_cb, &data, NULL);
  gst_pad_add_probe(srcpad, GST_PAD_PROBE_TYPE_BUFFER,
                    (GstPadProbeCallback) pad_probe_cb, &data, NULL);
  gst_pad_add_probe(srcpad, GST_PAD_PROBE_TYPE_DATA_BOTH,
                    (GstPadProbeCallback) pad_probe_cb, &data, NULL);

  /* Probes at the decoder's SINK pad create a segmentation fault*/
  // sinkpad = gst_element_get_static_pad(data.decoder, "sink");
  // gst_pad_add_probe(sinkpad, GST_PAD_PROBE_TYPE_QUERY_DOWNSTREAM,
  //                    (GstPadProbeCallback) pad_probe_cb, &data, NULL);

  /* Set the pipeline to "playing" state */
  gst_element_set_state(data.pipeline,GST_STATE_PLAYING);

  data.loop = g_main_loop_new(NULL, FALSE);
  g_main_loop_run(data.loop);

  /* clean up */
  gst_object_unref(srcpad);
  gst_element_set_state(data.pipeline, GST_STATE_NULL);
  gst_object_unref(GST_OBJECT(data.pipeline));
  g_source_remove(bus_watch_id);
  g_main_loop_unref(data.loop);

  return 0;
}
