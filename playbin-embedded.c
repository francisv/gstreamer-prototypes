/*
  Purpose:
  Prints all elements and their details used in a pipeline

  Compile:
  libtool --tag=CC --mode=link --quiet gcc -Wno-deprecated-declarations -g -O0 playbin-embedded.c -o playbin-embedded $(pkg-config --cflags --libs gstreamer-1.0)
  Debug:
  libtool --mode=execute gdb ... see shell script..

  Use:
  To use playbin3 (with decodebin3), do:

  export USE_PLAYBIN3=1

  When using decodebin3, update GST_DEBUG to decodebin3:n below

  To execute use shell script play-embedded.sh

  Other media to test:
  - Video with audio: sintel_trailer-480p.webm
  - DASH 264: http://dash.edgesuite.net/dash264/TestCases/1a/netflix/exMPD_BIP_TC1.mpd

  Source: http://docs.gstreamer.com/display/GstSDK/Playback+tutorial+7%3A+Custom+playbin2+sinks
*/

#include <config.h>
#include <gst/gst.h>
#include <string.h>
#include <tools/tools.h>

#define MAX_LENGTH 150

GST_DEBUG_CATEGORY_STATIC (my_category);
#define GST_CAT_DEFAULT my_category

static char *_name = NULL;

/* Structure to contain all our information, so we can pass it to callbacks */
typedef struct _CustomData {
  GstElement *pipeline, *bin_sink, *custom_sink, *filter;
  GstCaps *caps_filter, *caps_display, *caps_normalized;
  GstPad *pad, *ghost_pad, *pad_display;
  GstBus *bus;
  GstMessage *msg;
  guint bus_watch_id;
  GMainLoop *loop;
  GstStructure *structure;
  GstState state;
  GstStateChangeReturn ret;
  GstClockTime tfthen, tfnow, tflap;
  GstClockTimeDiff diff;
} CustomData;

/* Since Caps are normalized, each one contains only one structure.  This
 * functions prints them.  In addition, it prints features. */
void
print_caps_structure(GstCaps *caps_normalized){
  const gchar *ellipses;
  gchar *caps_str;
  GstStructure *structure;
  GstCapsFeatures *features;
  gint i;

  for (i = 0; i < gst_caps_get_size (caps_normalized); i++) {
    structure = gst_caps_get_structure (caps_normalized, i);
    features = gst_caps_get_features (caps_normalized, i);

    caps_str = gst_structure_to_string(structure);
    /* too long, ellipsize */
    if (strlen (caps_str) > MAX_LENGTH)
      ellipses = "…";
    else
      ellipses = "";

    GST_INFO("%d: %." G_STRINGIFY (MAX_LENGTH) "s%s",
             i + 1, caps_str, ellipses);
    g_free (caps_str);

    if (features && (gst_caps_features_is_any (features) ||
                     !gst_caps_features_is_equal (features,
                                                  GST_CAPS_FEATURES_MEMORY_SYSTEM_MEMORY))) {
      gchar *features_string = gst_caps_features_to_string (features);
      GST_INFO("Features: %s", features_string);
      g_free (features_string);
    }
  }
}

static GstCaps *
get_pad_caps (GstPad * pad)
{
  GstCaps *caps;

  /* first check the pad caps, if this is set, we are positively sure it is
   * fixed and exactly what the element will produce. */
  caps = gst_pad_get_current_caps (GST_PAD(pad));

  /* Then use the gst_pad_query_caps function if we don't have caps. These caps
   * might not be fixed in some cases, in which case analyze_new_pad will set up
   * a notify::caps signal to continue autoplugging. */
  if (!caps)
    caps = gst_pad_query_caps (GST_PAD(pad), NULL);

  return caps;
}

/* This function was extracted from ...utils.c.  This was not printing
 * everything as wanted.  Deprecated by print_element_properties_info */
static void
dump_get_object_params (GObject * object,
                        const char *const *ignored_propnames)
{
  gchar *param_name = NULL;
  GParamSpec **properties, *property;
  GValue value = { 0, };
  guint i, number_of_properties;
  gchar *tmp, *value_str;
  const gchar *ellipses;

  /* get paramspecs and show non-default properties */
  properties =
    g_object_class_list_properties (G_OBJECT_GET_CLASS (object),
                                    &number_of_properties);

  GST_INFO("Number of properties: %d",number_of_properties);
  if (properties) {
    for (i = 0; i < number_of_properties; i++) {
      gint j;
      gboolean ignore = FALSE;
      property = properties[i];

      /* skip some properties */
      if (!(property->flags & G_PARAM_READABLE))
        continue;
      if (!strcmp (property->name, "name"))
        continue;

      if (ignored_propnames)
        for (j = 0; ignored_propnames[j]; j++)
          if (!g_strcmp0 (ignored_propnames[j], property->name))
            ignore = TRUE;

      if (ignore)
        continue;

      g_value_init (&value, property->value_type);
      g_object_get_property (G_OBJECT (object), property->name, &value);
      /* bookmark: This if limits the printing of some properties.  What I have
       * to do now is: Ask the IRC group why elements do have NULL in capabilities? */
      if (!(g_param_value_defaults (property, &value))) {
        /* we need to serialise enums and flags ourselves to make sure the
         * enum/flag nick is used and not the enum/flag name, which would be the
         * C header enum/flag for public enums/flags, but for element-specific
         * enums/flags we abuse the name field for the property description,
         * and we don't want to print that in the dot file. The nick will
         * always work, and it's also shorter. */
        if (G_VALUE_HOLDS_ENUM (&value)) {
          GEnumClass *e_class = g_type_class_ref (G_VALUE_TYPE (&value));
          gint idx, e_val;

          tmp = NULL;
          e_val = g_value_get_enum (&value);
          for (idx = 0; idx < e_class->n_values; ++idx) {
            if (e_class->values[idx].value == e_val) {
              tmp = g_strdup (e_class->values[idx].value_nick);
              break;
            }
          }
          if (tmp == NULL) {
            g_value_unset (&value);
            continue;
          }
        } else if (G_VALUE_HOLDS_FLAGS (&value)) {
          GFlagsClass *f_class = g_type_class_ref (G_VALUE_TYPE (&value));
          GFlagsValue *vals = f_class->values;
          GString *s = NULL;
          guint idx, flags_left;

          s = g_string_new (NULL);

          /* we assume the values are sorted from lowest to highest value */
          flags_left = g_value_get_flags (&value);
          idx = f_class->n_values;
          while (idx > 0) {
            --idx;
            if (vals[idx].value != 0
                && (flags_left & vals[idx].value) == vals[idx].value) {
              if (s->len > 0)
                g_string_prepend_c (s, '+');
              g_string_prepend (s, vals[idx].value_nick);
              flags_left -= vals[idx].value;
              if (flags_left == 0)
                break;
            }
          }

          if (s->len == 0)
            g_string_assign (s, "(none)");

          tmp = g_string_free (s, FALSE);
        } else {
          tmp = g_strdup_value_contents (&value);
        }
        value_str = g_strescape (tmp, NULL);
        g_free (tmp);

        /* too long, ellipsize */
        if (strlen (value_str) > MAX_LENGTH)
          ellipses = "…";
        else
          ellipses = "";

        if (param_name)
          tmp = param_name;
        else
          tmp = (char *) "";

        param_name = g_strdup_printf ("%s: %."
                                      G_STRINGIFY (MAX_LENGTH) "s%s",
                                      property->name,
                                      value_str,
                                      ellipses);
        GST_INFO("%s",param_name);

        if (tmp[0] != '\0')
          g_free (tmp);

        g_free (value_str);
      }
      g_value_unset (&value);
    }
    g_free (properties);
  }
}

/* Needed by display_pads.  It prints instantiated capabilities. */
static void
dump_element_pads (GstIterator * pad_iter)
{
  GValue item = { 0, };
  gboolean pads_done;
  GstPad *pad;
  GstCaps *caps_display, *caps_normalized;
  GstStructure *structure;
  guint i;
  const gchar *ellipses;
  gchar *caps_str;

  /* Deprecated by print_element_properties_info */
  /* static const char *const ignore_propnames[] = */
  /*   { "parent", "template", "offset", "gropu-id", NULL */
  /*   }; */

  pads_done = FALSE;
  while (!pads_done) {
    switch (gst_iterator_next (pad_iter, &item)) {
    case GST_ITERATOR_OK:
      pad = g_value_get_object (&item);

      /* This function was extracted from ...utils.c.  This was not printing
       * everything as wanted.  Deprecated by print_element_properties_info */
      /* dump_get_object_params (G_OBJECT (pad), ignore_propnames); */

      /* Print caps */
      caps_display = get_pad_caps (pad);
      caps_normalized = gst_caps_normalize (caps_display);
      GST_INFO_OBJECT (pad, "Number of capabilities: %d",
                       gst_caps_get_size (caps_normalized));

      print_caps_structure(caps_normalized);

      g_value_reset (&item);
      break;
    case GST_ITERATOR_RESYNC:
    case GST_ITERATOR_ERROR:
    case GST_ITERATOR_DONE:
      pads_done = TRUE;
      break;
    }
  }
}

/* Needed by display_elements */
static void
display_pads(GstElement *element)
{
  GstIterator *pad_iter;

  if ((pad_iter = gst_element_iterate_pads (element))) {
    dump_element_pads (pad_iter);
    gst_iterator_free (pad_iter);
  }
}

/* Needed by original print_element_properties_info */
/* *INDENT-OFF* */
G_GNUC_PRINTF (1, 2)
/* *INDENT-ON* */
static void
n_print (const char *format, ...)
{
  va_list args;

  if (_name)
    g_print ("%s", _name);

  va_start (args, format);
  g_vprintf (format, args);
  va_end (args);
}

/* needed by print_element_properties_info */
static gboolean
print_field (GQuark field, const GValue * value, gpointer pfx)
{
  gchar *str = gst_value_serialize (value);
  GST_ERROR("In print_field");
  n_print ("%s  %15s: %s\n", (gchar *) pfx, g_quark_to_string (field), str);
  g_free (str);
  return TRUE;
}


/* needed by print_element_properties_info */
static void
print_caps (const GstCaps * caps, const gchar * pfx)
{
  guint i;

  g_return_if_fail (caps != NULL);

  if (gst_caps_is_any (caps)) {
    /* n_print ("%sANY\n", pfx); */
    GST_INFO ("Value: ANY");
    return;
  }
  if (gst_caps_is_empty (caps)) {
    /* n_print ("%sEMPTY\n", pfx); */
    GST_INFO("Value: EMPTY");
    return;
  }

  for (i = 0; i < gst_caps_get_size (caps); i++) {
    GstStructure *structure = gst_caps_get_structure (caps, i);
    GstCapsFeatures *features = gst_caps_get_features (caps, i);

    if (features && (gst_caps_features_is_any (features) ||
                     !gst_caps_features_is_equal (features,
                                                  GST_CAPS_FEATURES_MEMORY_SYSTEM_MEMORY))) {
      gchar *features_string = gst_caps_features_to_string (features);

      /* n_print ("%s%s(%s)\n", pfx, gst_structure_get_name (structure), */
      /* features_string); */
      GST_INFO("Value: %s(%s))", gst_structure_get_name(structure),features_string);

      g_free (features_string);
    } else {
      /* n_print ("%s%s\n", pfx, gst_structure_get_name (structure)); */
      GST_INFO("Value: %s",gst_structure_get_name (structure));
    }
    gst_structure_foreach (structure, print_field, (gpointer) pfx);
  }
}

/* needed by print_element_properties_info */
static gchar *
flags_to_string (GFlagsValue * vals, guint flags)
{
  GString *s = NULL;
  guint flags_left, i;

  /* first look for an exact match and count the number of values */
  for (i = 0; vals[i].value_name != NULL; ++i) {
    if (vals[i].value == flags)
      return g_strdup (vals[i].value_nick);
  }

  s = g_string_new (NULL);

  /* we assume the values are sorted from lowest to highest value */
  flags_left = flags;
  while (i > 0) {
    --i;
    if (vals[i].value != 0 && (flags_left & vals[i].value) == vals[i].value) {
      if (s->len > 0)
        g_string_append_c (s, '+');
      g_string_append (s, vals[i].value_nick);
      flags_left -= vals[i].value;
      if (flags_left == 0)
        break;
    }
  }

  if (s->len == 0)
    g_string_assign (s, "(none)");

  return g_string_free (s, FALSE);
}

/* needed by print_element_properties_info */
#define KNOWN_PARAM_FLAGS                                       \
  (G_PARAM_CONSTRUCT | G_PARAM_CONSTRUCT_ONLY |                 \
   G_PARAM_LAX_VALIDATION |  G_PARAM_STATIC_STRINGS |           \
   G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_DEPRECATED |   \
   GST_PARAM_CONTROLLABLE | GST_PARAM_MUTABLE_PLAYING |         \
   GST_PARAM_MUTABLE_PAUSED | GST_PARAM_MUTABLE_READY)

static void
number_of_properties(GstElement * element){
  GParamSpec **property_specs;
  guint num_properties;

  g_object_class_list_properties(G_OBJECT_GET_CLASS (element), &num_properties);

  GST_INFO ("-- Number of properties: %d --", num_properties);
}

/* Called from display_elements.  Sometimes I do not call this function */
static void
print_element_properties_info (GstElement * element)
{
  GParamSpec **property_specs;
  guint num_properties, i;
  gboolean readable;
  gboolean first_flag;

  property_specs = g_object_class_list_properties
    (G_OBJECT_GET_CLASS (element), &num_properties);
  GST_LOG ("Number of properties: %d", num_properties);

  for (i = 0; i < num_properties; i++) {
    GValue value = { 0, };
    GParamSpec *param = property_specs[i];

    readable = FALSE;

    g_value_init (&value, param->value_type);

    GST_INFO("Property %d: %s",i + 1, g_param_spec_get_name (param));

    /* n_print ("  %-20s: %s\n", g_param_spec_get_name (param), */
    /*     g_param_spec_get_blurb (param)); */

    first_flag = TRUE;
    /* n_print ("%-23.23s flags: ", ""); */
    if (param->flags & G_PARAM_READABLE) {
      g_object_get_property (G_OBJECT (element), param->name, &value);
      readable = TRUE;
      /* g_print ("%s%s\n", (first_flag) ? "" : ", ", _("readable")); */
      first_flag = FALSE;
    }
    else {
      /* if we can't read the property value, assume it's set to the default
       * (which might not be entirely true for sub-classes, but that's an
       * unlikely corner-case anyway) */
      GST_ERROR("Setting default value of property");
      g_param_value_set_default (param, &value);
    }
    if (param->flags & G_PARAM_WRITABLE) {
      /* g_print ("%s%s", (first_flag) ? "" : ", ", _("writable")); */
      first_flag = FALSE;
    }
    if (param->flags & G_PARAM_DEPRECATED) {
      /* g_print ("%s%s", (first_flag) ? "" : ", ", _("deprecated")); */
      first_flag = FALSE;
    }
    if (param->flags & GST_PARAM_CONTROLLABLE) {
      /* g_print (", %s", _("controllable")); */
      first_flag = FALSE;
    }
    if (param->flags & GST_PARAM_MUTABLE_PLAYING) {
      /* g_print (", %s", _("changeable in NULL, READY, PAUSED or PLAYING state")); */
    } else if (param->flags & GST_PARAM_MUTABLE_PAUSED) {
      /* g_print (", %s", _("changeable only in NULL, READY or PAUSED state")); */
    } else if (param->flags & GST_PARAM_MUTABLE_READY) {
      /* g_print (", %s", _("changeable only in NULL or READY state")); */
    }
    if (param->flags & ~KNOWN_PARAM_FLAGS) {
      g_print ("%s0x%0x", (first_flag) ? "" : ", ",
               param->flags & ~KNOWN_PARAM_FLAGS);
    }
    /* n_print ("\n"); */

    switch (G_VALUE_TYPE (&value)) {
    case G_TYPE_STRING:
      {
        const char *string_val = g_value_get_string (&value);

        /* n_print ("\n%-23.23s String. ", ""); */

        if (string_val == NULL)
          GST_INFO("Value: NULL");
        /* g_print ("\nDefault: null\n"); */
        else
          GST_INFO("Value: %s", string_val);
        /* g_print ("\nDefault: \"%s\"\n", string_val); */
        break;
      }
    case G_TYPE_BOOLEAN:
      {
        gboolean bool_val = g_value_get_boolean (&value);

        /* n_print ("%-23.23s Boolean. ", ""); */

        /* g_print ("Default: %s", bool_val ? "true" : "false"); */
        GST_INFO ("Value: %s", bool_val ? "true" : "false");
        break;
      }
      /* francisv: I did not change anything in case G_TYPE_ULONG */
    case G_TYPE_ULONG:
      {
        GParamSpecULong *pulong = G_PARAM_SPEC_ULONG (param);

        /* n_print ("%-23.23s Unsigned Long. ", ""); */
        g_print ("Range: %lu - %lu Default: %lu ",
                 pulong->minimum, pulong->maximum, g_value_get_ulong (&value));

        GST_ERROR ("%s: property '%s' of type ulong: consider changing to "
                   "uint/uint64", GST_OBJECT_NAME (element),
                   g_param_spec_get_name (param));
        break;
      }
      /* francisv: I did not change anything in case G_TYPE_LONG */
    case G_TYPE_LONG:
      {
        GParamSpecLong *plong = G_PARAM_SPEC_LONG (param);

        n_print ("%-23.23s Long. ", "");
        g_print ("Range: %ld - %ld Default: %ld ",
                 plong->minimum, plong->maximum, g_value_get_long (&value));

        GST_ERROR ("%s: property '%s' of type long: consider changing to "
                   "int/int64", GST_OBJECT_NAME (element),
                   g_param_spec_get_name (param));
        break;
      }
    case G_TYPE_UINT:
      {
        GParamSpecUInt *puint = G_PARAM_SPEC_UINT (param);

        /* n_print ("%-23.23s Unsigned Integer. ", ""); */
        /* g_print ("Range: %u - %u Default: %u ", */
        /* puint->minimum, puint->maximum, g_value_get_uint (&value)); */
        GST_INFO ("Value: %u, from range: %u - %u",
                  g_value_get_uint (&value), puint->minimum, puint->maximum);
        break;
      }
    case G_TYPE_INT:
      {
        GParamSpecInt *pint = G_PARAM_SPEC_INT (param);

        /* n_print ("%-23.23s Integer. ", ""); */
        /* g_print ("Range: %d - %d Default: %d ", */
        /*     pint->minimum, pint->maximum, g_value_get_int (&value)); */
        GST_INFO ("Value %d, from range: %d - %d",
                  g_value_get_int (&value), pint->minimum, pint->maximum);
        break;
      }
    case G_TYPE_UINT64:
      {
        GParamSpecUInt64 *puint64 = G_PARAM_SPEC_UINT64 (param);

        /* n_print ("%-23.23s Unsigned Integer64. ", ""); */
        /* g_print ("Range: %" G_GUINT64_FORMAT " - %" G_GUINT64_FORMAT */
        /*     " Default: %" G_GUINT64_FORMAT " ", */
        /* puint64->minimum, puint64->maximum, g_value_get_uint64 (&value)); */
        GST_INFO ("Value: %" G_GUINT64_FORMAT
                  ", from range: %" G_GUINT64_FORMAT " - %" G_GUINT64_FORMAT,
                  g_value_get_uint64 (&value), puint64->minimum, puint64->maximum);
        break;
      }
    case G_TYPE_INT64:
      {
        GParamSpecInt64 *pint64 = G_PARAM_SPEC_INT64 (param);

        /* n_print ("%-23.23s Integer64. ", ""); */
        /* g_print ("Range: %" G_GINT64_FORMAT " - %" G_GINT64_FORMAT */
        /*     " Default: %" G_GINT64_FORMAT " ", */
        /*     pint64->minimum, pint64->maximum, g_value_get_int64 (&value)); */
        GST_INFO ("Value: % " G_GINT64_FORMAT
                  ", from range: %" G_GINT64_FORMAT " - %" G_GINT64_FORMAT,
                  g_value_get_int64 (&value), pint64->minimum, pint64->maximum);
        break;
      }
    case G_TYPE_FLOAT:
      {
        GParamSpecFloat *pfloat = G_PARAM_SPEC_FLOAT (param);

        /* n_print ("%-23.23s Float. ", ""); */
        /* g_print ("Range: %15.7g - %15.7g Default: %15.7g ", */
        /*     pfloat->minimum, pfloat->maximum, g_value_get_float (&value)); */
        GST_INFO ("Value: %g, from range: %g - %g",
                  g_value_get_float (&value), pfloat->minimum, pfloat->maximum);
        break;
      }
    case G_TYPE_DOUBLE:
      {
        GParamSpecDouble *pdouble = G_PARAM_SPEC_DOUBLE (param);

        /* n_print ("%-23.23s Double. ", ""); */
        /* g_print ("Range: %15.7g - %15.7g Default: %15.7g ", */
        /*     pdouble->minimum, pdouble->maximum, g_value_get_double (&value)); */
        GST_INFO ("Value: %g, from range %g - %g",
                  g_value_get_double (&value), pdouble->minimum, pdouble->maximum);
        break;
      }
    case G_TYPE_CHAR:
    case G_TYPE_UCHAR:
      GST_ERROR ("%s: property '%s' of type char: consider changing to "
                 "int/string", GST_OBJECT_NAME (element),
                 g_param_spec_get_name (param));
      /* fall through */
    default:
      if (param->value_type == GST_TYPE_CAPS) {
        const GstCaps *caps = gst_value_get_caps (&value);

        if (!caps)
          GST_INFO("Value: NULL");
        /* n_print ("%-23.23s Caps (NULL)", ""); */
        else {
          print_caps (caps, "                           ");
        }
      }
      else if (G_IS_PARAM_SPEC_ENUM (param)) {
        GEnumValue *values;
        guint j = 0;
        gint enum_value;
        const gchar *value_nick = "";

        values = G_ENUM_CLASS (g_type_class_ref (param->value_type))->values;
        enum_value = g_value_get_enum (&value);

        while (values[j].value_name) {
          if (values[j].value == enum_value)
            value_nick = values[j].value_nick;
          j++;
        }

        /* n_print ("%-23.23s Enum \"%s\" Default: %d, \"%s\"", "", */
        /*     g_type_name (G_VALUE_TYPE (&value)), enum_value, value_nick); */
        GST_INFO ("Value: Enum \"%s\" %d, \"%s\"",
                  g_type_name (G_VALUE_TYPE (&value)), enum_value, value_nick);

        /* j = 0; */
        /* while (values[j].value_name) { */
        /*   g_print ("\n"); */
        /*   if (_name) */
        /*     g_print ("%s", _name); */
        /*   g_print ("%-23.23s    (%d): %-16s - %s", "", */
        /*       values[j].value, values[j].value_nick, values[j].value_name); */
        /*   j++; */
        /* } */
        /* g_type_class_unref (ec); */
      }
      else if (G_IS_PARAM_SPEC_FLAGS (param)) {
        GParamSpecFlags *pflags = G_PARAM_SPEC_FLAGS (param);
        GFlagsValue *vals;
        gchar *cur;

        vals = pflags->flags_class->values;

        cur = flags_to_string (vals, g_value_get_flags (&value));

        /* n_print ("%-23.23s Flags \"%s\" Default: 0x%08x, \"%s\"", "", */
        /*     g_type_name (G_VALUE_TYPE (&value)), */
        /*     g_value_get_flags (&value), cur); */

        GST_INFO ("Value: Flag \"%s\" 0x%08x, \"%s\"",
                  g_type_name (G_VALUE_TYPE (&value)),
                  g_value_get_flags (&value), cur);

        /* while (vals[0].value_name) { */
        /*   g_print ("\n"); */
        /*   if (_name) */
        /*     g_print ("%s", _name); */
        /*   g_print ("%-23.23s    (0x%08x): %-16s - %s", "", */
        /*       vals[0].value, vals[0].value_nick, vals[0].value_name); */
        /*   ++vals; */
        /* } */

        g_free (cur);
      }
      else if (G_IS_PARAM_SPEC_OBJECT (param)) {
        /* n_print ("%-23.23s Object of type \"%s\"", "", */
        /*          g_type_name (param->value_type)); */
        GST_INFO ("Value: TODO. Object type \"%s\"",
                  g_type_name (param->value_type));
      }
      else if (G_IS_PARAM_SPEC_BOXED (param)) {
        /* n_print ("%-23.23s Boxed pointer of type \"%s\"", "", */
        /* g_type_name (param->value_type)); */
        GST_INFO ("Value: Boxed pointer of type \"%s\"",
                  g_type_name (param->value_type));
        if (param->value_type == GST_TYPE_STRUCTURE) {
          const GstStructure *s = gst_value_get_structure (&value);
          if (s)
            gst_structure_foreach (s, print_field,
                                   (gpointer) "                           ");
        }
      }
      else if (G_IS_PARAM_SPEC_POINTER (param)) {
        if (param->value_type != G_TYPE_POINTER) {
          n_print ("%-23.23s Pointer of type \"%s\".", "",
                   g_type_name (param->value_type));
        } else {
          /* n_print ("%-23.23s Pointer.", ""); */
          GST_INFO ("TODO - Pointer.");
        }
      }
      else if (param->value_type == G_TYPE_VALUE_ARRAY) {
        GParamSpecValueArray *pvarray = G_PARAM_SPEC_VALUE_ARRAY (param);

        if (pvarray->element_spec) {
          n_print ("%-23.23s Array of GValues of type \"%s\"", "",
                   g_type_name (pvarray->element_spec->value_type));
        } else {
          n_print ("%-23.23s Array of GValues", "");
        }
      }
      else if (GST_IS_PARAM_SPEC_FRACTION (param)) {
        GstParamSpecFraction *pfraction = GST_PARAM_SPEC_FRACTION (param);

        n_print ("%-23.23s Fraction. ", "");

        g_print ("Range: %d/%d - %d/%d Default: %d/%d ",
                 pfraction->min_num, pfraction->min_den,
                 pfraction->max_num, pfraction->max_den,
                 gst_value_get_fraction_numerator (&value),
                 gst_value_get_fraction_denominator (&value));
      }
      else {
        n_print ("%-23.23s Unknown type %ld \"%s\"", "",
                 (glong) param->value_type, g_type_name (param->value_type));
      }
      break;
    }
    /* if (!readable) */
    /*   g_print (" Write only\n"); */
    /* else */
    /*   g_print ("\n"); */

    g_value_reset (&value);
  }
  if (num_properties == 0)
    n_print ("  none\n");

  g_free (property_specs);
}

/* Display elements in the parent pipeline recursively.  The output is sorted.
 * This function is called after message ASYNC_DONE in the bus_cb. */
static void
display_elements(GstElement *pipeline)
{
  GstElement *element;
  GstIterator *element_iter;
  gboolean elements_done = FALSE;
  GValue item = { 0, };
  gchar *element_name, *pipeline_name;

  pipeline_name = gst_element_get_name (GST_ELEMENT (pipeline));
  element_iter = gst_bin_iterate_sorted(GST_BIN(pipeline));
  while (!elements_done) {
    switch (gst_iterator_next (element_iter, &item)) {
    case GST_ITERATOR_OK:
      element = g_value_get_object (&item);
      element_name = gst_element_get_name (GST_ELEMENT (element));

      /* element is an internal pipeline */
      if(GST_IS_BIN(element)){
        GST_INFO("Subgraph: *%s* inside *%s* with elements:",element_name,pipeline_name);
        display_elements(element);
      }
      else {
        GST_INFO("Element \"%s\" instantiated",element_name);
        display_pads(element);
        number_of_properties(element);
        /* Uncomment to print all properties with GST_DEBUG:myapp:4 */
        print_element_properties_info(element);
      }
      g_value_reset (&item);
      break;
    case GST_ITERATOR_ERROR:
    case GST_ITERATOR_DONE:
      elements_done = TRUE;
      break;
    }
  }
  gst_iterator_free(element_iter);
}

/* Check messages in bus.  It creates graph and prints all elements inside the
 * PLAYING pipeline, including capabilities and properties */

static gboolean
bus_cb(GstBus *bus,
       GstMessage *msg,
       gpointer user_data)
{
  CustomData *data = user_data;

  switch (GST_MESSAGE_TYPE (msg)) {
  case GST_MESSAGE_ASYNC_DONE:{

    /* Get timestamp to know how much time it took the pipeline to be ready */
    data->tfnow = gst_util_get_timestamp ();
    data->diff = GST_CLOCK_DIFF (data->tfthen, data->tfnow);
    g_print (_("Total elapsed time: %" GST_TIME_FORMAT "\n"),
             GST_TIME_ARGS (data->diff));

    /* dump graph on preroll */
    GST_DEBUG_BIN_TO_DOT_FILE (GST_BIN (data->pipeline),
                               GST_DEBUG_GRAPH_SHOW_ALL, "playbin.async-done");

    /* Displays all element inside a pipeline */
    display_elements(data->pipeline);

    /* I got all what I wanted.  Let's quit the application */
    /* g_main_loop_quit (data->loop); */
    break;
  }
  case GST_MESSAGE_EOS:{
    g_print("EOS\n");
    g_main_loop_quit (data->loop);
    break;
  }
  case GST_MESSAGE_WARNING:{
    GError *err;
    gchar *dbg = NULL;

    /* dump graph on warning */
    GST_DEBUG_BIN_TO_DOT_FILE (GST_BIN (data->pipeline),
                               GST_DEBUG_GRAPH_SHOW_ALL, "playbin.warning");

    gst_message_parse_warning (msg, &err, &dbg);
    g_printerr ("WARNING %s\n", err->message);
    if (dbg != NULL)
      g_printerr ("WARNING debug information: %s\n", dbg);
    g_clear_error (&err);
    g_free (dbg);
    break;
  }
  case GST_MESSAGE_ERROR:{
    GError *err;
    gchar *dbg;

    /* dump graph on error */
    GST_DEBUG_BIN_TO_DOT_FILE_WITH_TS (GST_BIN (data->pipeline),
                                       GST_DEBUG_GRAPH_SHOW_ALL, "playbin.error");

    gst_message_parse_error (msg, &err, &dbg);
    g_printerr ("My error log: %s\n", err->message);
    if (dbg != NULL)
      g_printerr ("My error debug information: %s\n", dbg);
    g_clear_error (&err);
    g_free (dbg);

    /* flush any other error messages from the bus and clean up */
    gst_element_set_state (data->pipeline, GST_STATE_NULL);

    break;
  }
  default:
    /*unhandled message*/
    break;
  }
  return TRUE;
}

/* Displays capabilities of pads when signal notify::caps appears.  Elements in
 * element_setup_cb attach to this signal. */
static void
notify_caps_cb(GstPad *pad, GParamSpec * arg G_GNUC_UNUSED, CustomData *data){
  GstCaps *caps_display, *caps_normalized;
  GstStructure *structure;
  gint i;

  caps_display = get_pad_caps (pad);
  caps_normalized = gst_caps_normalize (caps_display);
  GST_INFO_OBJECT (pad, "SRC: %d dynamic",
                   gst_caps_get_size (caps_normalized));
  print_caps_structure(caps_normalized);
}

/* Gets elements as they are added to the pipeline.  These elements are already
 * negotiated and their capabilities are already filtered. */
void
element_setup_cb(GstElement *playbin, GstElement *child, CustomData *data){
  GstPad *pad_display;
  GstCaps *caps_display;
  GstCaps *caps_normalized;
  GstState state;

  /* Get timestamp to know how much time it takes up to the last element in the
   * pipeline */
  /* GST_WARNING_OBJECT(child, "Element added"); */
  /* data->tfnow = gst_util_get_timestamp (); */
  /* data->diff = GST_CLOCK_DIFF (data->tflap, data->tfnow); */
  /* data->tflap = data->tfnow; */

  /* g_print (_("Lap time: %" GST_TIME_FORMAT "\n"), */
  /*          GST_TIME_ARGS (data->diff)); */

  /* data->diff = GST_CLOCK_DIFF (data->tfthen, data->tfnow); */
  /* g_print (_("Elapsed time: %" GST_TIME_FORMAT "\n"), */
  /*          GST_TIME_ARGS (data->diff)); */


  /* Tests state */
  gst_element_get_state (child, &state, NULL, GST_CLOCK_TIME_NONE);
  GST_LOG_OBJECT (child, "State: %s", gst_element_state_get_name (state));

  pad_display = gst_element_get_static_pad (child, "sink");
  if (pad_display != NULL) {
    g_assert (GST_IS_PAD (pad_display));
    caps_display = get_pad_caps (pad_display);
    caps_normalized = gst_caps_normalize (caps_display);
    GST_INFO_OBJECT (child, "added with SINK: %d static",
                     gst_caps_get_size (caps_normalized));

    print_caps_structure(caps_normalized);

  } else
    GST_INFO_OBJECT (child, "added with SINK: N/A static");

  pad_display = gst_element_get_static_pad (child, "src");
  if (pad_display != NULL) {
    g_assert (GST_IS_PAD (pad_display));
    caps_display = get_pad_caps (pad_display);
    caps_normalized = gst_caps_normalize (caps_display);
    GST_INFO_OBJECT (child, "added with SRC: %d static",
                     gst_caps_get_size (caps_normalized));

    print_caps_structure(caps_normalized);

    g_signal_connect(pad_display, "notify::caps", G_CALLBACK(notify_caps_cb), data);
  } else
    GST_INFO_OBJECT (child, "added with SRC: N/A static");

  /* There are no instructions to unrefer pads in the documentation
   * gst_object_unref(pad_display);
   *
   * Unref a GstCaps and and free all its structures and the structures' values
   * when the refcount reaches 0.  gst_caps_unref(caps_display);
   * gst_caps_unref(caps_normalized);
   *
   * Freeing structure throws error
   * The structure must not have a parent when this function is called!
   * gst_structure_free(structure);
   */
}

int main(int argc, char *argv[]) {
  CustomData *data;
  gint i = 0;
  gboolean no_match = TRUE;
  GstPluginFeature *feature;

  GST_DEBUG_CATEGORY_INIT (my_category, "my_app", GST_DEBUG_BG_GREEN,
                           "This is the debug category for my playbin-embedded.");

  data = g_new0 (CustomData, 1);

  /* Initialize GStreamer */
  gst_init (&argc, &argv);

  feature = gst_registry_find_feature (gst_registry_get (),
                                       "jackaudiosink", GST_TYPE_ELEMENT_FACTORY);
  gst_plugin_feature_set_rank(feature,GST_RANK_SECONDARY);

  feature = gst_registry_find_feature (gst_registry_get (),
                                       "oss4sink", GST_TYPE_ELEMENT_FACTORY);
  gst_plugin_feature_set_rank(feature,GST_RANK_SECONDARY);

  feature = gst_registry_find_feature (gst_registry_get (),
                                       "osssink", GST_TYPE_ELEMENT_FACTORY);
  gst_plugin_feature_set_rank(feature,GST_RANK_SECONDARY);

  feature = gst_registry_find_feature (gst_registry_get (),
                                       "filesrc", GST_TYPE_ELEMENT_FACTORY);
  gst_plugin_feature_set_rank(feature,GST_RANK_SECONDARY);

  if (argc != 2) {
    g_printerr ("Usage: %s URI\n\n", argv[0]);
    return 1;
  }

  /* Initial timestamp */
  data->tfthen = gst_util_get_timestamp ();
  data->tflap = data->tfthen;

  /* Build the pipeline */
  data->pipeline = gst_parse_launch ( "playbin" ,NULL);

  g_signal_connect(data->pipeline, "element-setup", G_CALLBACK(element_setup_cb),
                   data);

  g_object_set(G_OBJECT(data->pipeline), "uri", argv[1], NULL);

  /* **************************
     Create my own sink:
     - pulsesink
     - alsasink
     - openalsink
  ****************************/

  /* data->custom_sink = gst_element_factory_make("openalsink","my_custom_sink"); */
  /* if (!data->custom_sink){ */
  /*   GST_ERROR_OBJECT(data->custom_sink,"Creation error"); */
  /* } */

  /* Print all what pulsesink can consume */
  /* data->pad_display = gst_element_get_static_pad (data->custom_sink, "sink"); */
  /* data->caps_display = get_pad_caps(data->pad_display); */
  /* data->caps_normalized = gst_caps_normalize(data->caps_display); */

  /* GST_DEBUG_OBJECT(data->pad_display,"1. Number of possible SINK INPUTs: %d",gst_caps_get_size(data->caps_normalized)); */
  /* for (i = 0; i < gst_caps_get_size (data->caps_normalized); i++) { */
  /*   data->structure = gst_caps_get_structure (data->caps_normalized, i);  */
  /*   GST_INFO ("%d. %s\n", i+1, gst_structure_to_string(data->structure)); */
  /* } */

  /* START part for checking whether gst_caps_can_intersect limits the
   * capabilities of an element */

  /* /\* Create filter *\/ */
  /* data->filter = gst_element_factory_make("capsfilter","my_filter"); */
  /* g_assert (data->filter); */
  /* data->caps_filter = gst_caps_new_simple ("audio/x-raw", */
  /*                                          "format", G_TYPE_STRING, "S24_32BE", */
  /*                                          NULL); */
  /* g_object_set (data->filter, "caps", data->caps_filter, NULL); */

  /* /\* If autodetect has been provided with filter caps, */
  /*  * accept only elements that match with the filter caps *\/ */
  /* if (data->caps_filter) { */

  /*   data->pad_display = gst_element_get_static_pad (data->custom_sink, "sink"); */
  /*   data->caps_display = gst_pad_query_caps (data->pad_display, NULL); */
  /*   GST_DEBUG_OBJECT (data->pad_display, */
  /*                     "Checking caps: %" GST_PTR_FORMAT " vs. %" GST_PTR_FORMAT, */
  /*                     data->caps_filter, data->caps_display); */
  /*   no_match = !gst_caps_can_intersect (data->caps_filter, data->caps_display); */

  /*   if (no_match) { */
  /*     GST_DEBUG_OBJECT (data->custom_sink, "Incompatible caps"); */
  /*   } else { */
  /*     GST_DEBUG_OBJECT (data->custom_sink, "Found compatible caps"); */
  /*   } */
  /* } */

  /* /\* END test for limiting capabilities *\/ */

  /* /\* change state of custom_sink *\/ */
  /* data->ret = gst_element_set_state(data->custom_sink,GST_STATE_READY); */
  /* if (data->ret == GST_STATE_CHANGE_SUCCESS) { */
  /*   gst_element_get_state(data->custom_sink, &data->state, NULL, GST_CLOCK_TIME_NONE); */
  /*   GST_LOG_OBJECT(data->custom_sink,"State: %s",gst_element_state_get_name(data->state)); */

  /*   GST_LOG_OBJECT(data->pad_display,"2. Number of possible SINK INPUTs: %d",gst_caps_get_size(data->caps_normalized)); */

  /*   for (i = 0; i < gst_caps_get_size (data->caps_normalized); i++) { */
  /*     data->structure = gst_caps_get_structure (data->caps_normalized, i); */
  /*     GST_LOG ("%d. %s", i+1, gst_structure_to_string(data->structure)); */
  /*   } */
  /* } */

  /* /\* Create bin audio sink *\/ */
  /* data->bin_sink = gst_bin_new ("my_audio_sink_bin"); */
  /* gst_bin_add_many(GST_BIN(data->bin_sink), data->filter, data->custom_sink, NULL); */
  /* /\* link *\/ */
  /* if (!gst_element_link(data->filter, data->custom_sink)) { */
  /*   g_warning ("Failed to link elements!"); */
  /* } */

  /* data->ret = gst_element_set_state(data->pipeline,GST_STATE_READY); */
  /* if (data->ret != GST_STATE_CHANGE_SUCCESS) { */
  /*   GST_ERROR("Error changing state of pipeline"); */
  /* } */

  /* gst_element_get_state(data->custom_sink, &data->state, NULL, GST_CLOCK_TIME_NONE); */
  /* GST_DEBUG_OBJECT(data->custom_sink,"Checking state after linking and pipeline ready: %s",gst_element_state_get_name(data->state)); */

  /* GST_DEBUG_OBJECT(data->pad_display,"3. Number of possible SINK INPUTs: %d",gst_caps_get_size(data->caps_normalized)); */

  /* for (i = 0; i < gst_caps_get_size (data->caps_normalized); i++) { */
  /*   data->structure = gst_caps_get_structure (data->caps_normalized, i); */
  /*   GST_DEBUG ("%d. %s", i+1, gst_structure_to_string(data->structure)); */
  /* } */

  /* /\* TODO: automate to gst_element_request_pad for sinks that are not always (static) *\/ */
  /* data->pad = gst_element_get_static_pad (data->filter, "sink"); */
  /* data->ghost_pad = gst_ghost_pad_new ("sink", data->pad); */
  /* gst_pad_set_active (data->ghost_pad, TRUE); */
  /* gst_element_add_pad(data->bin_sink, data->ghost_pad); */
  /* /\* gst_object_unref(data->pad); *\/ */

  /* Set custom_sink to be the default sink */
  /* g_object_set (GST_OBJECT (data->pipeline), "audio-sink", data->bin_sink, NULL); */
  /* g_object_set (GST_OBJECT (data->pipeline), "audio-sink", data->custom_sink, NULL); */

  /* Start playing */
  /* francisv: if the pipeline does not want to change state, see how
   * gst-launch.c:1139 catches those errors */
  gst_element_set_state (data->pipeline, GST_STATE_PLAYING);

  data->bus = gst_element_get_bus (data->pipeline);
  data->bus_watch_id = gst_bus_add_watch(data->bus, bus_cb, data);
  data->loop = g_main_loop_new (NULL, FALSE);
  g_main_loop_run(data->loop);

  GST_LOG("Cleaning...");

  //g_object_unref(data->pad_display);
  //gst_caps_unref(data->caps_normalized);
  //gst_caps_unref(data->caps_display);

  /* Setting pipeline to GST_STATE_NULL throws error:
     (lt-playbin-embedded:8853): GStreamer-CRITICAL **: gst_mini_object_unref: assertion 'mini_object->refcount > 0' failed
     TODO: to learn how to debug this
  */
  //gst_element_set_state (data->pipeline, GST_STATE_NULL);

  if (data->msg != NULL)
    gst_message_unref (data->msg);
  gst_object_unref (data->bus);
  g_source_remove (data->bus_watch_id);
  gst_object_unref (data->pipeline);
  g_main_loop_unref(data->loop);
  g_free(data);

  return 0;
}
