//  example7-1.c
//  gstreamer_manual
//
//  Created by francisv on 26/02/14.

#include <gst/gst.h>

/* Structure to contain all our information, so we can pass it to callbacks */
typedef struct _CustomData{
  GMainLoop *loop;
  GMainContext *context; /* GLib context used to run the main loop */
  GstElement *pipeline;
  GstElement *source;
  GstElement *demuxer;
  GstElement *decoder;
  GstElement *sink;
} CustomData;

/* My callback to read all messages and signals */
static gboolean
my_bus_callback(GstBus *bus, GstMessage *message, CustomData *data){
  g_print("Got *%s* message from *%s*\n", GST_MESSAGE_TYPE_NAME(message),
          GST_MESSAGE_SRC_NAME(message));
  // GST_MESSAGE_SIGNAL(message);

  switch (GST_MESSAGE_TYPE(message)){
  case GST_MESSAGE_ERROR: {
    GError *err;
    gchar *debug;

    gst_message_parse_error(message, &err, &debug);
    g_print("Error: %s\n", err->message);
    g_error_free(err);
    g_free (debug);

    g_main_loop_quit(data->loop);
    break;
  }
  case GST_MESSAGE_EOS:
    /* end-of-stream */
    g_main_loop_quit(data->loop);
    break;
  default:
    /* unhandled message */
    break;
  }

  /* we want to be notified again the next time there is a message
   * on the bus, so returning TRUE (FALSE means we want to stop watching
   * for messages on the bus and our callback should not be called again)
   */
  return TRUE;
}

static void on_pad_added(GstElement *element, GstPad *pad, CustomData *data){
  GstPad *sinkpad;

  /* we can now link this pad with the vorbis-decoder sink pad */
  GST_DEBUG ("Dynamic pad created, linking demuxer/decoder");

  sinkpad = gst_element_get_static_pad(data->decoder, "sink");

  gst_pad_link(pad, sinkpad);

  gst_object_unref(sinkpad);
}


gint main (gint argc, gchar *argv[]){
  CustomData data;
  GstBus *bus;
  guint bus_watch_id;
  // GSource *bus_source;

  /*  int */
  gst_init(&argc, &argv);

  /* create pipeline */
  data.pipeline = gst_pipeline_new("my_pipeline");
  data.source = gst_element_factory_make ("filesrc", "source");
  data.demuxer = gst_element_factory_make ("oggdemux", "demuxer");
  data.decoder = gst_element_factory_make ("vorbisdec", "decoder");
  data.sink = gst_element_factory_make ("pulsesink", "sink");

  if (!data.pipeline){
    g_printerr("pipeline element could not be created. Exiting.\n");
    return -1;
  }else if (!data.source){
    g_printerr("source element could not be created. Exiting.\n");
    return -1;
  }else if (!data.demuxer){
    g_printerr("demuxer element could not be created. Exiting.\n");
    return -1;
  }else if (!data.decoder){
    g_printerr("decoder element could not be created. Exiting.\n");
    return -1;
  }else if (!data.sink){
    g_printerr("sink element could not be created. Exiting.\n");
    return -1;
  }

  /* We set the input filename to the source element */
  g_object_set(data.source, "location",
               "/media/sf_mnt/media/Hydrate-Kenny_Beltrey.ogg", NULL);

  /* adds a watch for new message on our pipeline's message bus to
   * the default GLib main context, which is the main context that our
   * GLib main loop is attached to below
   */
  bus = gst_pipeline_get_bus(GST_PIPELINE(data.pipeline));
  // gst_bus_enable_sync_message_emission(bus);
  bus_watch_id = gst_bus_add_watch(bus, (GstBusFunc)my_bus_callback, &data);

  // bus_source = gst_bus_create_watch (bus);
  // g_source_set_callback (bus_source, (GSourceFunc) gst_bus_async_signal_func,
  //                       NULL, NULL);
  // g_source_attach (bus_source, NULL);
  // g_source_unref (bus_source);

  gst_object_unref(bus);

  gst_bin_add_many(GST_BIN(data.pipeline), data.source, data.demuxer,
                   data.decoder, data.sink, NULL);

  gst_element_link(data.source,data.demuxer);
  gst_element_link_many(data.decoder,data.sink,NULL);

  g_signal_connect(data.demuxer, "pad-added", G_CALLBACK(on_pad_added),
                   &data);

  /* Set the pipeline to "playing" state */
  gst_element_set_state(data.pipeline,GST_STATE_PLAYING);

  /* create a mainloop that runs/iterates the default GLib main context
   * (context NULL), in other words: makes the context check if anything
   * it watches for has happened. When a message has been posted on the
   * bus, the default main context will automatically call our
   * my_bus_callback() function to notify us of that message.
   * The main loop will be run until someone calls g_main_loop_quit()
   */

  data.loop = g_main_loop_new(NULL, FALSE);
  g_main_loop_run(data.loop);

  /* clean up */
  gst_element_set_state(data.pipeline, GST_STATE_NULL);
  gst_object_unref(GST_OBJECT(data.pipeline));
  g_source_remove(bus_watch_id);
  g_main_loop_unref(data.loop);

  return 0;
}
