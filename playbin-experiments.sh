#!/bin/bash
# Runs playbin-experiments

export GST_DEBUG_DUMP_DOT_DIR=/home/francisv/Dropbox/gstreamer/graphs/dots/last
export USE_PLAYBIN3=1
/home/francisv/gitlab/my-applications/playbin-experiments file:///home/francisv/Dropbox/media/sintel_trailer-480p.webm

# GST_URI, urisourcebin code needs to add capabilities, features and properties
# parsebin needs properties and features
# apparently playsinkaudioconvert and pulse are not needed or work there is missing
# export GST_DEBUG=latency:9
# export GST_DEBUG=autodetect:4,playsink:4,my_app:4,pulse:3,playbin3:4,playbin:4,GST_REGISTRY:4,GST_ELEMENT_FACTORY:4,decodebin:4,decodebin3:4,GST_URI:4,parsebin:4,urisourcebin:4,playsinkaudioconvert:4
# export GST_DEBUG=GST_URI:4,autodetect:4
# export GST_DEBUG=*:9
# export GST_DEBUG=my_app:3
# export GST_DEBUG=playbin:4

## Uncomment for latency tracing
#export GST_DEBUG="GST_TRACER:7"
#export GST_TRACERS=latency
#export G_DEBUG=fatal_warnings

# Experiment 7
# Runs GStreamer from master without profiling
# echo "Running experiment 7"
# for i in `seq 1 100`;
# do /home/francisv/my-applications/playbin-experiments file:///home/francisv/media/Hydrate-Kenny_Beltrey.ogg  >> experiment-7.log
# done

# Experiment 8
# Runs GStreamer from master without profiling
# echo "Running experiment 8"
# for i in `seq 1 100`;
# do /home/francisv/my-applications/playbin-experiments file:///home/francisv/media/sintel_trailer-480p.webm >> experiment-8.log
# done

# plays video from file:
#/home/francisv/my-applications/playbin-embedded file:///home/francisv/media/sintel_trailer-480p.webm

# plays video with subtitles from DASH:
# TODO

## GDB debug:

# audio:
# libtool --mode=execute gdb --args /home/francisv/my-applications/playbin-embedded file:///home/francisv/media/Hydrate-Kenny_Beltrey.ogg

# video:
# libtool --mode=execute gdb --args /home/francisv/my-applications/playbin-embedded file:///home/francisv/media/sintel_trailer-480p.webm
