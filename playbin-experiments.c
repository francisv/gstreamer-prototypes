/*
  Purpose:
  Runs playbin and measures time to create pipeline

  Compile:
  /* libtool --tag=CC --mode=link --quiet gcc -Wno-deprecated-declarations -g -O0 playbin-experiments.c -o playbin-experiments $(pkg-config --cflags --libs gstreamer-1.0) */
  Debug:
  libtool --mode=execute gdb ... see shell script..

  Use:
  To use playbin3 (with decodebin3), do:

  export USE_PLAYBIN3=1

  When using decodebin3, update GST_DEBUG to decodebin3:n below

  To execute use shell script play-embedded.sh

  Other media to test:
  - Video with audio: sintel_trailer-480p.webm
  - DASH 264: http://dash.edgesuite.net/dash264/TestCases/1a/netflix/exMPD_BIP_TC1.mpd

  Source: http://docs.gstreamer.com/display/GstSDK/Playback+tutorial+7%3A+Custom+playbin2+sinks
*/

#include <config.h>
#include <gst/gst.h>
#include <string.h>
#include <tools/tools.h>

#define MAX_LENGTH 150

GST_DEBUG_CATEGORY_STATIC (my_category);
#define GST_CAT_DEFAULT my_category

static char *_name = NULL;

/* Structure to contain all our information, so we can pass it to callbacks */
typedef struct _CustomData {
  GstElement *pipeline, *bin_sink, *custom_sink, *filter;
  GstCaps *caps_filter, *caps_display, *caps_normalized;
  GstPad *pad, *ghost_pad, *pad_display;
  GstBus *bus;
  GstMessage *msg;
  guint bus_watch_id;
  GMainLoop *loop;
  GstStructure *structure;
  GstState state;
  GstStateChangeReturn ret;
  GstClockTime start, end;
  GstClockTimeDiff diff;
} CustomData;

/* Check messages in bus.  It creates graph and prints all elements inside the
 * PLAYING pipeline, including capabilities and properties */

static gboolean
bus_cb(GstBus *bus,
         GstMessage *msg,
         gpointer user_data)
{
  CustomData *data = user_data;

  switch (GST_MESSAGE_TYPE (msg)) {
  case GST_MESSAGE_ASYNC_DONE:{

    /* Get timestamp to know how much time it took the pipeline to be ready */
    data->end = gst_util_get_timestamp ();
    data->diff = GST_CLOCK_DIFF (data->start, data->end);
    g_print (_("%" GST_TIME_FORMAT "\n"),
           GST_TIME_ARGS (data->diff));

    /* dump graph on preroll */
    GST_DEBUG_BIN_TO_DOT_FILE (GST_BIN (data->pipeline),
                               GST_DEBUG_GRAPH_SHOW_ALL, "playbin.async-done");

    /* I got all what I wanted.  Let's quit the application */
    g_main_loop_quit (data->loop);
    break;
  }
  case GST_MESSAGE_EOS:{
    g_print("EOS\n");
    g_main_loop_quit (data->loop);
    break;
  }
  case GST_MESSAGE_WARNING:{
    GError *err;
    gchar *dbg = NULL;

    /* dump graph on warning */
    GST_DEBUG_BIN_TO_DOT_FILE (GST_BIN (data->pipeline),
                               GST_DEBUG_GRAPH_SHOW_ALL, "playbin.warning");

    gst_message_parse_warning (msg, &err, &dbg);
    g_printerr ("WARNING %s\n", err->message);
    if (dbg != NULL)
      g_printerr ("WARNING debug information: %s\n", dbg);
    g_clear_error (&err);
    g_free (dbg);
    break;
  }
  case GST_MESSAGE_ERROR:{
    GError *err;
    gchar *dbg;

    /* dump graph on error */
    GST_DEBUG_BIN_TO_DOT_FILE_WITH_TS (GST_BIN (data->pipeline),
                                       GST_DEBUG_GRAPH_SHOW_ALL, "playbin.error");

    gst_message_parse_error (msg, &err, &dbg);
    g_printerr ("My error log: %s\n", err->message);
    if (dbg != NULL)
      g_printerr ("My error debug information: %s\n", dbg);
    g_clear_error (&err);
    g_free (dbg);

    /* flush any other error messages from the bus and clean up */
    gst_element_set_state (data->pipeline, GST_STATE_NULL);

    break;
  }
  default:
    /*unhandled message*/
    break;
  }
  return TRUE;
}

int main(int argc, char *argv[]) {
  CustomData *data;
  gint i = 0;
  gboolean no_match = TRUE;
  GstPluginFeature *feature;

  GST_DEBUG_CATEGORY_INIT (my_category, "my_app", GST_DEBUG_BG_GREEN,
                           "This is the debug category for my playbin-experiments.");

  data = g_new0 (CustomData, 1);

  /* Initialize GStreamer */
  gst_init (&argc, &argv);

  if (argc != 2) {
    g_printerr ("Usage: %s URI\n\n", argv[0]);
    return 1;
  }

  /* Initial timestamp */
  data->start = gst_util_get_timestamp ();

  /* Build the pipeline */
  data->pipeline = gst_parse_launch ( "playbin" ,NULL);

  g_object_set(G_OBJECT(data->pipeline), "uri", argv[1], NULL);

  /* Start playing */
  /* If the pipeline does not want to change state, see how
   * gst-launch.c:1139 catches those errors */
  gst_element_set_state (data->pipeline, GST_STATE_PLAYING);

  data->start = gst_util_get_timestamp ();

  data->bus = gst_element_get_bus (data->pipeline);
  data->bus_watch_id = gst_bus_add_watch(data->bus, bus_cb, data);
  data->loop = g_main_loop_new (NULL, FALSE);
  g_main_loop_run(data->loop);

  if (data->msg != NULL)
    gst_message_unref (data->msg);
  gst_object_unref (data->bus);
  g_source_remove (data->bus_watch_id);
  gst_object_unref (data->pipeline);
  g_main_loop_unref(data->loop);
  g_free(data);

  return 0;
}
